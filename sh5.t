$GLOBAL_AHEADER$
<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 $GLOBAL_MENU$
 <!-- ============= Тело ==================-->
 <div class="col m12 l9 ">
 
 
 $GLOBAL_RECLAMA$
 <!-- <body> --><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td style="white-space: nowrap;">$SHOP_PATH$</td>
 <?if($ADD_ENTRY_LINK$)?><td align="right" style="white-space: nowrap;">[ <?if($PAGE_ID$=='invoices')?><a href="/panel/?a=shop;l=order" target=_blank><!--<s5457>-->Настройки<!--</s>--></a><?else?><a href="$ADD_ENTRY_LINK$"><!--<s4391>-->Добавить товар<!--</s>--></a><?endif?> ]</td><?endif?>
 </tr></table>
 
 <div id="cont-$MODULE_ID$-$PAGE_ID$"<?if($PAGE_ID$!='invoices')?> style="position:relative;"<?endif?>>
 
 <?if($ORDERS_GEOMAP$)?><div style="float:right;">$ORDERS_GEOMAP$</div><?endif?>
 <?if($ORDERS_EXPORT$)?><div style="float:right; padding:0 3px;">$ORDERS_EXPORT$</div><?endif?>
 
 <h1>$TITLE$</h1>
 
 <?if($ORDER_NOTICE$)?>$ORDER_NOTICE$<?endif?>
 
 <?if($PAGE_ID$=='invoices')?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
 <td><!--<s4532>-->Заказов<!--</s>-->: <b>$NUM_ENTRIES$</b> | <!--<s4538>-->Оборот<!--</s>-->: <b>$TOTAL$</b> <?if($PROFIT$)?>| <!--<s4546>-->Прибыль<!--</s>-->: <b>$PROFIT$</b><?endif?><td>
 <td align="right"><!--<s4535>-->Со статусом<!--</s>-->: $STATUS_SELECTOR$</td></tr></table>
 <hr />
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>$DATE_SELECTOR$</td><td>$SUM_SELECTOR$</td><td style="width:1%; text-align:right; padding-right:4px;">$ARCHIVE_SELECTOR$</td><?if($USER_SELECTOR$)?><td width="1%" align="right" nowrap>$USER_SELECTOR$</td><?endif?></tr></table>
 <hr />
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>$ORDERS_SEARCH$</td><td align="right">$PAYMENT_SELECTOR$ $DELIVERY_SELECTOR$</td></tr></table>
 <?if($DELIVDATE_SELECTOR$)?><hr />
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>$DELIVDATE_SELECTOR$</td><?if($COURIER_SELECTOR$)?><td align="right">$COURIER_SELECTOR$</td><?endif?></tr></table><?endif?>
 <hr />
 <?endif?>
 
 <?if($PAGE_ID$=='price')?>
 <?if($NUM_PAGES$>1)?><div style="text-align:right; padding-bottom:3px;"><!--<s3015>-->Страницы<!--</s>-->: $PAGE_SELECTOR$</div><?endif?>
 <form id="shop-price-form" action="" onsubmit="return price2Basket()">
 <input type="hidden" name="mode" value="grp-add">
 <table class="gTable" width="100%" border="0" cellspacing="1" cellpadding="2">
 <tr><td class="gTableTop"><!--<s4374>-->Наименование<!--</s>--></td><td class="gTableTop" width="1%"><!--<s4371>-->Цена<!--</s>--></td><td class="gTableTop" width="1%"><!--<s4504>-->Количество<!--</s>--></td></tr>
 <?endif?>
 
 $BODY$
 
 <?if($PAGE_ID$=='price')?>
 </table>
 <div style="text-align:center; padding:10px;"><input type="submit" class="pinput" value="Добавить в корзину"> <input type="button" class="pinput" onclick="location.href='/shop/checkout'" value="Оформить заказ"></div>
 </form>
 <?endif?>
 
 <?if($PAGE_ID$=='checkout')?>
 <?if($NUM_ENTRIES$)?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%" id="total-sum"><tr valign="top">
 <?if($ORDER_TAX$)?><td><h4><!--<s4607>-->Такса за доставку<!--</s>--></h4> $ORDER_TAX$</td><?endif?>
 <?if($ORDER_DISCOUNT$)?><td><h4><!--<s4608>-->Скидка по дисконту<!--</s>--></h4> <strike>$ORDER_TOTAL$</strike> ($ORDER_DISCOUNT$)</td><?endif?>
 <td><h4><!--<s4570>-->Сумма к оплате выбранным способом<!--</s>--></h4> $ORDER_TOPAY_CURR$</td> </tr></table><div id="order-submit">$ORDER_BUTTON$</div><br>
 <h2><!--<s4452>-->Способ оплаты<!--</s>--></h2>
 $PAYMENT_LIST$
 
 
 $ORDER_FIELDS$
 <div id="order-submit">$ORDER_BUTTON$</div><br>
 $ORDER_INFO$
 
 Внимательно прочтите <a href=http://mqlsoft.net/index/uslovija_pri_pokupke_tovara/0-11>Условия покупки</a> товаров перед совершением оплаты.
 Нажимая кнопку "Оплатить" Вы соглашаетесь с условиями приобретения товара.<br><br>
 
 
 
 <?else?><div style="text-align:center; line-height:30px; padding-bottom:30px;"><h3><!--<s5434>-->Ваша корзина пуста<!--</s>--></h3>[ <a href="/shop"><!--<s4451>-->Продолжить покупки<!--</s>--></a> ]</div><?endif?>
 <?endif?>
 
 <?if($PAGE_SELECTOR$)?><div style="text-align:center; padding:10px;">$PAGE_SELECTOR$</div><?endif?>
 
 </div>
 <!-- </body> -->
 </div>
 </div>
</div>



<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$