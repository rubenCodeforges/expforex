<div class="row">
 <div class="col s12 m12">
 <div class="card">
 <div class="card-content black-text">
 <span class="card-title"><a href="$ENTRY_URL$">$TITLE$</a></span> 
 
 <p>$MESSAGE$</p>
 <div class="col s12 center-align"> 
 <?if($OTHER1$)?> <a class="btn" href="$OTHER1$"><i class="material-icons left">new_releases</i>Ссылка на материал</a><br><br><?endif?>
 </div>
 <br>
 </div>
 <div class="card-action">
 <div class="row">
 <div class="col s4 "> 
 <?if($CATEGORY_NAME$)?> <a href="$CATEGORY_URL$"><i class="material-icons left">dashboard</i>$CATEGORY_NAME$</a> <?endif?>
 </div>
 <div class="col s4 "> 
 <i class="material-icons left">date_range</i> <span title="$TIME$"> $DATE$ </span>
 </div>
 
 <div class="col s4 "> 
 <?if($LOADS$)?> <span> <i class="material-icons left">attach_file</i> $LOADS$</span><?else?>
 <span> <i class="material-icons left">remove_red_eye</i> $READS$</span><?endif?> 
 <?if($MODER_PANEL$)?>$MODER_PANEL$<?endif?> 
 </div>
 </div>
 </div>
 </div>
 </div>
</div>