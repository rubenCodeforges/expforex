$GLOBAL_AHEADER$

<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->





<div class="container">
 <div class="row">
 <div class="col s12">
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <div class="row">
 <div class="col s12 m3 center-align"> 
 <br> <a href="$ENTRY_URL$"><?if($SCREEN_URL$)?> <img class="responsive-img" src="$SCREEN_URL$"><?else?><?if($IMG_URL1$)?> <img class="responsive-img" src="$IMG_URL1$"><?else?><img class="responsive-img" src="/1NEWDESIGN/NEWIMAGES/NOIMAGE.jpg"><?endif?><?endif?></a> 
 </div>
 <div class="col s12 m9 center-align"> 
 <?if($MODER_PANEL$)?>$MODER_PANEL$<?endif?> <br>
 <h2>$ENTRY_TITLE$</h2>
 
 <?if($LOAD$)?><a class="btn pulse" href="$FILE_URL$"><i class="material-icons left">file_download</i>Скачать</a><?endif?>
 </div>
 </div>
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Боковая панель справа START -->
 <!-- ====================================================================================================== -->
 
 <div class="row">
 <div class="col s12 m3 z-depth-5">
 <div class="col s12">
 
 <br>
 
 <i class="material-icons left">date_range</i> <span title="$TIME$">Добавлено: $ADD_DATE$ </span> <br> <br>
 <i class="material-icons left">date_range</i> <span title="$TIME$">Обновлено: $DATE$ </span> <br> <br>
 <?if($LOADS$)?> <i class="material-icons left">attach_file</i> Загрузок: $LOADS$<br><br><?else?><i class="material-icons left">remove_red_eye</i>Просмотров: $READS$<br><br><?endif?> 
 <?if($CAT_NAME$)?> <a href="$CAT_URL$"><i class="material-icons left">dashboard</i>Категория: $CAT_NAME$</a> <br><br><?endif?> 
 <?if($SOURCE$)?> <a href="$SOURCE$"><i class="material-icons left">surround_sound</i>Источник</a> <br><br><?endif?> 
 
 
 <?if($OTHER7$)?> <i class="material-icons left">merge_type</i>Тип: $OTHER7$ <br><br><?endif?> 
 <?if($OTHER8$)?> <i class="material-icons left">open_with</i>Особенности: $OTHER8$ <br><br><?endif?> 
 <?if($OTHER9$)?> <i class="material-icons left">computer</i>Терминал: $OTHER9$ <br><br><?endif?> 
 
 <?if($TAGS$)?> <i class="material-icons left">cloud</i>$TAGS$<br><br><?endif?> 
 
 
 </div>
 
 
 <div class="col s12 center-align">
 <br>
 <?if($OTHER10$)?> <a class="btn" href="$OTHER10$"><i class="material-icons left">settings</i>Входные параметры </a><br><br><?endif?> 
 <?if($OTHER2$)?><a class="btn" href="$OTHER2$">
 <?if($LOADS$)?>
 <i class="material-icons left">fullscreen</i>Полное описание
 <?else?><i class="material-icons left">file_download</i>Скачать
 <?endif?><?endif?> </a> <br><br>
 <?if($OTHER3$)?> <a class="btn" href="$OTHER3$"><i class="material-icons left">forum</i>Обсуждение</a> <br><br><?endif?>
 <?if($OTHER11$)?> <a class="btn" href="$OTHER11$"><i class="material-icons left">new_releases</i>Вопрос ответ </a><br><br><?endif?>
 
 
 <?if($OTHER4$)?> <a class="btn" href="$OTHER4$"><i class="material-icons left">mode_edit</i>История версий</a> <br><br><?endif?> 
 <?if($OTHER5$)?> <a class="btn" href="$OTHER5$"><i class="material-icons left">shopping_cart</i>Купить лицензию с mql5.com</a> <br><br><?endif?> 
 <?if($OTHER12$)?> <a class="btn" href="$OTHER12$"><i class="material-icons left">file_download</i>Скачать с mql5.com</a> <br><br><?endif?> 
 <?if($OTHER6$)?> <a class="btn" href="$OTHER6$"><i class="material-icons left">add_shopping_cart</i>Купить Открытый код</a> <br><br><?endif?> 
 
 
 </div>
 
 <div class="center-align"> <br><br>$ADVBT_1$<br></div>
 </div>
 <!-- ====================================================================================================== -->
 <!-- Боковая панель справа END -->
 <!-- ====================================================================================================== -->
 
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Центральная панель с информацией START -->
 <!-- ====================================================================================================== -->
 
 
 <div class="col s12 m8 push-m1"> 
 
 <p align=left>$MESSAGE$</p> 
 
 <div class="col s12 center-align"> 
 <?if($OTHER1$)?> <a class="btn" href="$OTHER1$"><i class="material-icons left">new_releases</i>Ссылка на материал</a><br><br><?endif?>
 </div>
 </div> 
 
 </div>
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Комментарии START -->
 <!-- ====================================================================================================== --> 
 
 <?if($COM_IS_ACTIVE$)?>
 <?if($COM_CAN_READ$)?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr><td width="60%" height="25"><!--<s5183>-->Всего отзывов<!--</s>-->: <b>$COM_NUM_ENTRIES$</b></td><td align="right" height="25">$COM_PAGE_SELECTOR$</td></tr>
 <tr><td colspan="2">$COM_BODY$</td></tr>
 <tr><td colspan="2" align="center">$COM_PAGE_SELECTOR1$</td></tr>
 <tr><td colspan="2" height="10"></td></tr>
 </table>
 <?endif?>
 
 <?if($COM_CAN_ADD$)?>
 $COM_ADD_FORM$
 <?else?>
 <?if($USER_LOGGED_IN$)?><?else?><div align="center" class="commReg"><!--<s5237>-->Добавлять отзывы могут только зарегистрированные пользователи.<!--</s>--><br />[ <a href="$REGISTER_LINK$"><!--<s3089>-->Регистрация<!--</s>--></a> | <a href="$LOGIN_LINK$"><!--<s3087>-->Вход<!--</s>--></a> ]</div><?endif?>
 <?endif?>
 <?endif?>
 
 
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Похожие записи START -->
 <!-- ====================================================================================================== -->
 
 <div class="row hide-on-small-only">
 <div class="col s12">
 <div class="section">
 <h5>Похожие записи</h5>
 
 <div class="collection"><?$RELATED_ENTRIES$(10)?></div>
 </div>
 <font style="font-size: 1pt; "> Здесь можно <a href="$ENTRY_URL$">скачать $ENTRY_TITLE$</a><br>
 <a href="$ENTRY_URL$">Купить $ENTRY_TITLE$</a> можно здесь - 
 <a href="$ENTRY_URL$">Прочитать о $ENTRY_TITLE$</a> можно здесь - 
 <a href="$ENTRY_URL$">Обсуждение $ENTRY_TITLE$</a> - стейты, отзывы, комментарии - 
 <a href="$ENTRY_URL$">Отзывы о $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Настройки к $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Мониторинг $ENTRY_TITLE$ </a> - 
 
 <a href="$ENTRY_URL$">Download $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Buy $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Read about $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Discusion of $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Comments of $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Settings $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Monitoring $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ скачать бесплатно</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ ключ</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ открытый код</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ скачать</a></font> -
 
 
 
 </div>
 </div>
 <!-- ====================================================================================================== -->
 <!-- Похожие записи END -->
 <!-- ====================================================================================================== -->
 
 
 </div>
 </div>
</div>
<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$