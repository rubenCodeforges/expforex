$GLOBAL_AHEADER$
<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 $GLOBAL_MENU$
 <!-- ============= Тело ==================-->
 <div class="col m12 l9 ">
 
 
 $GLOBAL_RECLAMA$
 <!-- <body> --><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td nowrap>$SHOP_PATH$</td><td align="right" nowrap><b><!--<s4521>-->Статус<!--</s>-->:</b> $ORDER_STATUS$</td></tr></table>
 <h1><!--<s4717>-->Заказ<!--</s>--> <!--<s4453>-->№<!--</s>--> $ORDER_CODE$</h1>
 $BODY$
 <?if($DIGITAL_ITEM_1$)?>
 <div style="text-align:center; font-size:1.6em; padding:15px; line-height:150%;"><?$DIGITAL_GOODS$('<!--<s5481>-->Получить<!--</s>--> %NAME% %SIZE%')?></div>
 <?endif?>
 $ORDER_PRINT$
 <?if($DELIVERY$)?>
 <h3 style="margin:5px 0;"><!--<s4454>-->Способ доставки<!--</s>--></h3>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td width="20">&nbsp;</td>
 <td><b>$DELIVERY$</b><div style="padding-top:5px">$DELIVERY_DESCR$</div></td>
 <?if($ORDER_TAX$)?><td style="text-align:right;padding-left:20px; font-size:16px;"><?if($ORDER_TAX_EDIT$)?>$ORDER_TAX_EDIT$<?else?>$ORDER_TAX$<?endif?></td><?endif?>
 </tr></table>
 <?endif?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;">
 <?if($ORDER_DISCOUNT$)?><tr><td><h3><!--<s4608>-->Скидка по дисконту<!--</s>--></h3></td><td style="text-align:right;padding-left:20px; font-size:16px;">$ORDER_DISCOUNT$</td></tr><?endif?>
 <tr valign="top">
 <td><h3><!--<s4513>-->К оплате<!--</s>--></h3></td>
 <td style="text-align:right;padding-left:20px; font-size:16px;"><?if($ORDER_DISCOUNT$)?><strike>$ORDER_TOTAL$</strike><br><?endif?>$ORDER_TOPAY$</td>
 </tr>
 </table>
 
 <h2><!--<s4604>-->Оплата заказа<!--</s>--></h2>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr valign="top">
 <td width="20">&nbsp;</td><td><b>$PAYMENT$</b><div style="padding-top:5px">$PAYMENT_DESCR$</div></td>
 <?if($PAY_NOW$)?><td style="text-align:right;padding-left:20px;"><h3>$ORDER_TOPAY_CURR$</h3>$PAY_NOW$</td><?endif?>
 </tr></table>
 
 <?if($ORDER_FIELDS$)?><div id="order-info" class="commFl" style="margin-top:20px;">$ORDER_FIELDS$</div><?endif?>
 
 <div style="padding:20px 0; text-align:right; font-style: italic;"><b><!--<s4522>-->Добавлен<!--</s>-->:</b> $ADD_DATE$, $ADD_TIME$ <?if($MOD_TIME$)?> <b><!--<s4523>-->Изменён<!--</s>-->:</b> $MOD_DATE$, $MOD_TIME$<?endif?></div>
 <!-- </body> -->
 </div>
 </div>
</div>



<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$