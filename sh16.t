<html>
 <head>
 <meta name="robots" content="noindex, nofollow">
 <meta http-equiv="content-type" content="text/html; charset=UTF-8">
 <title>Заказ #$ORDER_ID$ - $MODULE_NAME$</title>
 </head>
 <body style="background-color:#FFFFFF; font-family: Times New Roman, Arial, Tahoma; color: #000000">
 <style type="text/css">
 table td { font-size: 12px }
 .nowrap{ white-space: nowrap }
 </style>
 <table style="width: 180mm; height: 145mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr valign="top">
 <td style="border-right: medium none; border-width: 1pt medium medium 1pt; border-style: solid none none solid; border-color: rgb(0, 0, 0) -moz-use-text-color -moz-use-text-color rgb(0, 0, 0); width: 50mm; height: 70mm;" align="center">
 <br><strong>Извещение</strong><br>
 <span style="font-size: 53mm;">&nbsp;</span><br>
 <strong>Кассир</strong>
 </td>
 <td style="border-right: 1pt solid rgb(0, 0, 0); border-width: 1pt 1pt medium; border-style: solid solid none; border-color: rgb(0, 0, 0) rgb(0, 0, 0) -moz-use-text-color;" align="center">
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td align="right"><small><em>Форма № ПД-4</em></small></td>
 </tr>
 <tr>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0); font-size:10px;"><p><strong>$RECIPIENT$</strong></p></td>
 </tr>
 <tr>
 <td align="center"><small>(наименование получателя платежа)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td style="width: 37mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_INN$</td>
 <td style="width: 9mm;">&nbsp;</td>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_ACCAUNT$</td>
 </tr>
 <tr>
 <td align="center"><small>(ИНН получателя платежа)</small></td>
 <td><small>&nbsp;</small></td>
 <td align="center"><small>(номер счета получателя платежа)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td></td>
 <td style="width: 73mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK$</td>
 <td align="right">БИК&nbsp;&nbsp;</td>
 <td style="width: 33mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK_BIK$</td>
 </tr>
 <tr>
 <td>&nbsp;</td>
 <td align="center"><small>(наименование банка получателя платежа)</small></td>
 <td>&nbsp;</td>
 <td>&nbsp;</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Номер кор./сч. банка получателя платежа&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK_ACCAUNT$</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td style="width: 60mm; border-bottom: 1pt solid rgb(0, 0, 0);"><small>$PAYMENT_DSCR$</small></td>
 <td style="width: 2mm;">&nbsp;</td>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;</td>
 </tr>
 <tr>
 <td align="center"><small>(наименование платежа)</small></td>
 <td><small>&nbsp;</small></td>
 <td align="center"><small>(номер лицевого счета (код) плательщика)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Ф.И.О. плательщика&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);"><small>$RECEIPT_PAYER$</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Адрес плательщика&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);"><small>$RECEIPT_PAYER_ADDR$</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td>
 Сумма платежа&nbsp;
 <span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_TOTAL1$&nbsp;</span>&nbsp;
 $ORDER_CURRENCY$&nbsp;
 <span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_TOTAL2$&nbsp;</span>&nbsp;
 $ORDER_CURRENCY_CENTS$
 </td>
 <td align="right">&nbsp;&nbsp;Сумма платы за услуги&nbsp;&nbsp;_____&nbsp;$ORDER_CURRENCY$&nbsp;____&nbsp;$ORDER_CURRENCY_CENTS$</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td>Итого&nbsp;&nbsp;_______&nbsp;$ORDER_CURRENCY$&nbsp;____&nbsp;$ORDER_CURRENCY_CENTS$</td>
 <td align="right">&nbsp;&nbsp;«<span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_DATE_DAY$&nbsp;</span>»<span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_DATE_MONTH_TXT$&nbsp;</span>&nbsp;<span style="border-bottom: 1pt solid rgb(0, 0, 0);">$ORDER_DATE_YEAR$</span>&nbsp;г.</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td><small>С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td align="right"><strong>Подпись плательщика&nbsp;_____________________</strong></td>
 </tr>
 </tbody>
 </table>
 <br/>
 </td>
 </tr>
 <tr valign="top">
 <td style="border-right: medium none; border-width: 1pt medium 1pt 1pt; border-style: solid none solid solid; border-color: rgb(0, 0, 0) -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); width: 50mm; height: 70mm;" align="center">
 <span style="font-size: 50mm;">&nbsp;<br></span>
 <strong>Квитанция</strong><br>
 <span style="font-size: 8pt;">&nbsp;<br></span><strong>Кассир</strong>
 </td>
 <td style="border-right: 1pt solid rgb(0, 0, 0); border-width: 1pt; border-style: solid; border-color: rgb(0, 0, 0);" align="center">
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td align="right"><small><em>Форма № ПД-4</em></small></td>
 </tr>
 <tr>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0); font-size:10px;"><p><strong>$RECIPIENT$</strong></p></td>
 </tr>
 <tr>
 <td align="center"><small>(наименование получателя платежа)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td style="width: 37mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_INN$</td>
 <td style="width: 9mm;">&nbsp;</td>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_ACCAUNT$</td>
 </tr>
 <tr>
 <td align="center"><small>(ИНН получателя платежа)</small></td>
 <td><small>&nbsp;</small></td>
 <td align="center"><small>(номер счета получателя платежа)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td></td>
 <td style="width: 73mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK$</td>
 <td align="right">БИК&nbsp;&nbsp;</td>
 <td style="width: 33mm; border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK_BIK$</td>
 </tr>
 <tr>
 <td>&nbsp;</td>
 <td align="center"><small>(наименование банка получателя платежа)</small></td>
 <td>&nbsp;</td>
 <td>&nbsp;</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Номер кор./сч. банка получателя платежа&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);">$PAYEE_BANK_ACCAUNT$</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td style="width: 60mm; border-bottom: 1pt solid rgb(0, 0, 0);"><small>$PAYMENT_DSCR$</small></td>
 <td style="width: 2mm;">&nbsp;</td>
 <td style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;</td>
 </tr>
 <tr>
 <td align="center"><small>(наименование платежа)</small></td>
 <td><small>&nbsp;</small></td>
 <td align="center"><small>(номер лицевого счета (код) плательщика)</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Ф.И.О. плательщика&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);"><small>$RECEIPT_PAYER$</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td width="1%" class="nowrap">Адрес плательщика&nbsp;&nbsp;</td>
 <td width="100%" style="border-bottom: 1pt solid rgb(0, 0, 0);"><small>$RECEIPT_PAYER_ADDR$</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td>
 Сумма платежа&nbsp;
 <span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_TOTAL1$&nbsp;</span>&nbsp;
 $ORDER_CURRENCY$&nbsp;
 <span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_TOTAL2$&nbsp;</span>&nbsp;
 $ORDER_CURRENCY_CENTS$
 </td>
 <td align="right">&nbsp;&nbsp;Сумма платы за услуги&nbsp;&nbsp;_____&nbsp;$ORDER_CURRENCY$&nbsp;____&nbsp;$ORDER_CURRENCY_CENTS$</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td>Итого&nbsp;&nbsp;_______&nbsp;$ORDER_CURRENCY$&nbsp;____&nbsp;$ORDER_CURRENCY_CENTS$</td>
 <td align="right">&nbsp;&nbsp;«<span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_DATE_DAY$&nbsp;</span>»<span style="border-bottom: 1pt solid rgb(0, 0, 0);">&nbsp;$ORDER_DATE_MONTH_TXT$&nbsp;</span>&nbsp;<span style="border-bottom: 1pt solid rgb(0, 0, 0);">$ORDER_DATE_YEAR$</span>&nbsp;г.</td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td><small>С условиями приема указанной в платежном документе суммы, в т.ч. с суммой взимаемой платы за услуги банка, ознакомлен и согласен.</small></td>
 </tr>
 </tbody>
 </table>
 <table style="margin-top: 3pt; width: 122mm;" border="0" cellpadding="0" cellspacing="0">
 <tbody>
 <tr>
 <td align="right"><strong>Подпись плательщика&nbsp;_____________________</strong></td>
 </tr>
 </tbody>
 </table>
 <br/>
 </td>
 </tr>
 </tbody>
 </table>
 </body>
 </html>