<?if($CAT_FL$)?>
 <label>$CAT_SIGN$:</label> $CAT_FL$ <hr /><?endif?>

<ul class="form-fields">
 
 <?if($TITLE_FL$)?>
 <li><label for="title">$TITLE_SIGN$:</label> <li>$TITLE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($HGU_TITLE_FL$)?>
 <li><label for="chpu">$HGU_TITLE_SIGN$:</label> <li>$HGU_TITLE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($SITE_DIRECT_URL_FL$)?>
 <li><label for="chpu">$SITE_DIRECT_URL_SIGN$:</label> <li>$SITE_DIRECT_URL_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($BRIEF_FL$)?>
 <li class="notcolumn">$BRIEF_FL$ <li class="notcolumn"> <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($MESSAGE_FL$)?>
 <li class="notcolumn">$MESSAGE_FL$ <li class="notcolumn"> <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($IMAGES_FL$)?>
 <li><label for="file1">$IMAGES_SIGN$</label> <li>$IMAGES_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($ATTACHMENTS_FL$)?>
 <li><label for="show_attachments">$ATTACHMENTS_SIGN$</label> <li>$ATTACHMENTS_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($VERSION_FL$)?>
 <li><label for="title">$VERSION_SIGN$:</label> <li>$VERSION_FL$<?endif?>

 <?if($LICENSE_FL$)?>
 <li><label for="licence">$LICENSE_SIGN$</label> <li>$LICENSE_FL$<?endif?>

 <?if($OS_FL$)?>
 <li><label for="os">$OS_SIGN$</label> <li>$OS_FL$<?endif?>

 <?if($LANGUAGE_FL$)?>
 <li><label for="lng">$LANGUAGE_SIGN$</label> <li>$LANGUAGE_FL$<?endif?>

 <?if($SCREEN_FL$)?>
 <li><label for="screen">$SCREEN_SIGN$:</label> <li>$SCREEN_FL$<?endif?>

 <?if($FILE_FL$)?>
 <li><label for="file">$FILE_SIGN$:</label> <li>$FILE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($REMOTEFILE_FL$)?>
 <li><label for="llink">$REMOTEFILE_SIGN$:</label> <li>$REMOTEFILE_FL$<?endif?>

 <?if($REMOTESIZE_FL$)?>
 <li><label for="asite">$REMOTESIZE_SIGN$:</label> <li>$REMOTESIZE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($NAME_FL$)?>
 <li><label for="aname">$NAME_SIGN$:</label> <li>$NAME_FL$<?endif?>

 <?if($EMAIL_FL$)?>
 <li><label for="aemail">$EMAIL_SIGN$:</label> <li>$EMAIL_FL$<?endif?>

 <?if($PHONE_FL$)?>
 <li><label for="phone">$PHONE_SIGN$:</label> <li>$PHONE_FL$<?endif?>

 <?if($SITE_FL$)?>
 <li><label for="asite">$SITE_SIGN$:</label> <li>$SITE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($SOURCE_FL$)?>
 <li><label for="source">$SOURCE_SIGN$:</label> <li>$SOURCE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($DOCPAGE_FL$)?>
 <li><label for="doclink">$DOCPAGE_SIGN$:</label> <li>$DOCPAGE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <!-- OTHER1..5 -->
 <?if($OTHER1_FL$)?>
 <li><label for="other1">$OTHER1_SIGN$:</label> <li>$OTHER1_FL$<?endif?>

 <?if($OTHER2_FL$)?>
 <li><label for="other2">$OTHER2_SIGN$:</label> <li>$OTHER2_FL$<?endif?>

 <?if($OTHER3_FL$)?>
 <li><label for="other3">$OTHER3_SIGN$:</label> <li>$OTHER3_FL$<?endif?>

 <?if($OTHER4_FL$)?>
 <li><label for="other4">$OTHER4_SIGN$:</label> <li>$OTHER4_FL$<?endif?>

 <?if($OTHER5_FL$)?>
 <li><label for="other5">$OTHER5_SIGN$:</label> <li>$OTHER5_FL$<?endif?>
 <!-- /OTHER1..5 -->

 <!-- FREE EXTRAFIELDS -->
 <?if($OTHER6_FL$)?>
 <li><label for="other6">$OTHER6_SIGN$:</label> <li>$OTHER6_FL$<?endif?>

 <?if($OTHER7_FL$)?>
 <li><label for="other7">$OTHER7_SIGN$:</label> <li>$OTHER7_FL$<?endif?>

 <?if($OTHER8_FL$)?>
 <li><label for="other8">$OTHER8_SIGN$:</label> <li>$OTHER8_FL$<?endif?>
 <!-- /FREE EXTRAFIELDS -->

 <?if($OTHER1_FL$ or $OTHER2_FL$ or $OTHER3_FL$ or $OTHER4_FL$ or $OTHER5_FL$
 or $OTHER6_FL$ or $OTHER7_FL$ or $OTHER8_FL$)?><li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($FILTER1_FL$)?>
 <li><label>$FILTER1_SIGN$:</label> <li>$FILTER1_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($FILTER2_FL$)?>
 <li><label>$FILTER2_SIGN$:</label> <li>$FILTER2_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($FILTER3_FL$)?>
 <li><label>$FILTER3_SIGN$:</label> <li>$FILTER3_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($TAGS_FL$)?>
 <li><label for="tags">$TAGS_SIGN$:</label> <li>$TAGS_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($ORDERING_FL$)?>
 <li><label for="sort">$ORDERING_SIGN$:</label> <li>$ORDERING_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($ADD_TIME_FL$)?>
 <li><label for="ya">$ADD_TIME_SIGN$:</label> <li>$ADD_TIME_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($PLACED_FL$)?>
 <li><label>$PLACED_SIGN$:</label> <li>$PLACED_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($WHO_ADDED_FL$)?>
 <li><label >$WHO_ADDED_SIGN$:</label> <li>$WHO_ADDED_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($TOPPERIOD_FL$)?>
 <li><label for="isontop">$TOPPERIOD_SIGN$:</label> <li>$TOPPERIOD_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($COM_NOTICE_FL$)?>
 <li><label >$COM_NOTICE_SIGN$:</label> <li>$COM_NOTICE_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($MODER_OPT_FL$)?>
 <li><label >$MODER_OPT_SIGN$:</label> <li>$MODER_OPT_FL$ <li class="notcolumn"><hr /><li class="notcolumn"><?endif?>

 <?if($META_FIELDS$)?>$META_FIELDS$<?endif?>

 <?if($CROSSPOST_FORM$)?>$CROSSPOST_FORM$<?endif?>

 <?if($SECURE_FL$)?>
 <li><label for="code">$SECURE_SIGN$:</label> <li>$SECURE_FL$ <li class="security notcolumn"><hr /><li class="notcolumn"><?endif?>

</ul>
<center>$SUBMIT_FL$
$CANCEL_FL$</center>