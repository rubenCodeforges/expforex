$GLOBAL_AHEADER$
<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 $GLOBAL_MENU$
 <!-- ============= Тело ==================-->
 <div class="col m12 l9 ">
 <!-- <body> -->
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr>
 <td style="white-space: nowrap;">$SHOP_PATH$</td>
 <?if($ADD_ENTRY_LINK$)?><td align="right" style="white-space: nowrap;">[ <a href="$ADD_ENTRY_LINK$"><!--<s4391>-->Добавить товар<!--</s>--></a> ]</td><?endif?>
 </tr>
 </table>
 
 <h1>$_USERNAME$</h1>
 
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr valign="top">
 
 <?if $_AVATAR$?><td style="padding-right:10px; width:1%">$_AVATAR$</td><?endif?>
 
 <td>
 
 <?if $_SHOP_ACTIVITY_URL$?><a href="$_SHOP_ACTIVITY_URL$"><!--<s5428>-->Товары<!--</s>--></a>&nbsp;<small>($_SHOP_ENTRIES$)</small><?endif?>
 <?if $_SHOP_COMM_URL$?>| <a href="$_SHOP_COMM_URL$"><!--<s4783>-->Обсуждения($COM_NUM_ENTRIES$)<!--</s>--></a><?endif?>
 <?if $_SHOP_WISHLIST_URL$?>| <a href="$_SHOP_WISHLIST_URL$"><!--<s7231>-->Список желаний<!--</s>--></a><?endif?>
 <?if $_SHOP_TRANSACTIONS_URL$?><?if($_SHOP_ACTIVITY_URL$ || $_SHOP_COMM_URL$ || $_SHOP_WISHLIST_URL$)?>| <?endif?><a href="/shop/user/$USER_ID$"><!--<s4928>-->Баланс<!--</s>--></a> | <a href="$_SHOP_TRANSACTIONS_URL$"><!--<s4962>-->Движения средств<!--</s>--></a><?endif?>
 
 <ul class="shop-options">
 <?if($_ICQ$)?><li><span class="opt">ICQ:</span> <span class="val">$_ICQ$</span></li><?endif?>
 <?if($_AOL$)?><li><span class="opt">AOL:</span> <span class="val">$_AOL$</span></li><?endif?>
 <?if($_WWW$)?><li><span class="opt">WWW:</span> <span class="val"><a href="$_WWW$" target=_blank>$_WWW$</a></span></li><?endif?>
 
 </ul>
 
 </td></tr>
 </table>
 
 <?if($PAGE_ID$ == 'usertrans')?>
 
 <hr />
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>$DATE_SELECTOR$</td><td align="right">$TRANS_SELECTOR$</td></tr></table>
 <hr />
 
 <?endif?>
 
 <?if($PAGE_ID$ == 'userarea')?>
 
 <?if($BALANCE$)?></li>
 <h3 class="postFirst" id="shop-balance"><!--<s4928>-->Баланс<!--</s>--></h3>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
 <td><div class="eTitle">$BALANCE$</div></td>
 <td style="text-align:right; line-height:150%">$ADD_FUNDS$</td>
 </tr></table>
 
 <?endif?>
 
 <?if($WITHDRAWAL_PURSE$)?></li>
 <h3 class="postFirst" id="shop-paymetns-purse"><!--<s4930>-->Счёт для выплаты<!--</s>--></h3>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
 <td>$WITHDRAWAL_PURSE$</td>
 <?if($WITHDRAWAL_LAST_DATE$)?><td style="width:1%; white-space:nowrap;"><!--<s4931>-->Последняя выплата<!--</s>-->:<div style="text-align:right;font-style:italic;">$WITHDRAWAL_LAST_DATE$, $WITHDRAWAL_LAST_TIME$</div></td><?endif?>
 </tr></table>
 <div class="forumDescr" style="padding:10px 0"><!--<s4935>-->Минимальная сумма для выплаты<!--</s>-->: $WITHDRAWAL_MIN$</div>
 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
 <td>$WITHDRAWAL_MODE$</td>
 <td style="width:1%; white-space:nowrap;">$WITHDRAWAL_REQUEST$</td>
 </tr></table>
 <?endif?>
 
 <?endif?>
 
 $BODY$
 
 <?if($PAGE_SELECTOR$)?><div style="text-align:center; padding:10px;">$PAGE_SELECTOR$</div><?endif?>
 
 <!-- </body> -->
 </div>
 </div>
</div>



<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$