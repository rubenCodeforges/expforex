$GLOBAL_AHEADER$

<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($SHOP_PATH$)?> <a href="http://www.expforex.com/shop" class="breadcrumb"> MQL Market</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 $GLOBAL_MENU$
 <!-- ============= Тело ==================-->
 <div class="col m12 l9 ">
 
 <!-- <body> -->
 
 <h1 class="page-title">$CAT_NAME$</h1>
 
 <div class="shop-cat-descr shop-cat-big with-clear">
 <?if($CAT_IMG$)?>
 <img src="$CAT_IMG$" alt="$CAT_NAME$">
 <?endif?>
 $CAT_DESCR$
 </div>
 
 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-filter">
 <tr>
 <div align="center">Страницы - <?$DRAW_BLOCK$('PAGE_SELECTOR','plist')?></div><br>
 <td><!--<s4419>-->Доступно позиций<!--</s>-->: <b><?$DRAW_BLOCK$('NUM_ENTRIES','ne_cont')?></b><td>
 <td align="right"><?$DRAW_BLOCK$('SORT_SELECTOR','slist')?></td>
 </tr>
 </table>
 
 <div class="row">
 $BODY$
 </div>
 
 
 <div align="center">Страницы - $PAGE_MORE$ 
 <?$DRAW_BLOCK$('PAGE_PREV','pprev')?> 
 <?$DRAW_BLOCK$('PAGE_SELECTOR','plist')?> 
 <?$DRAW_BLOCK$('PAGE_NEXT','pnext')?></div>
 <!-- </body> -->
 </div>
 </div>
 </div>
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Тело страницы END -->
 <!-- ====================================================================================================== -->
 $GLOBAL_BFOOTER$