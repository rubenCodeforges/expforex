<html>
 <head>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8">
 <title>Заказ #$ORDER_ID$ - $MODULE_NAME$</title>
 </head>
 <body style="background-color:#FFFFFF;">
 <center>
 <h1>$MODULE_NAME$</h1>
 <div>$HOME_PAGE_LINK$</div>
 <h2>Заказ #$ORDER_ID$</h2>
 </center>
 <p>Время оформления заказа: <b>$ADD_WDAY$, $ADD_DATE$ $ADD_TIME$</b></p>
 <p>Способ оплаты: <b>$PAYMENT$</b></p>
 <?if($DELIVERY$)?><p>Способ доставки: <b>$DELIVERY$</b></p><?endif?>
 <?if($ORDER_UID$)?><p>Покупатель: <b>$USER_FULL_NAME$</b></p>
 <p>Email: <b>$_EMAIL$</b></p><?endif?>
 $ORDER_FIELDS$
 <h3><u>
 Заказанный товар</u></h3>
 <table width="100%" cellspacing="0" cellpadding="5" border="1">
 <tr>
 <th>№</th>
 <th>Артикул</th>
 <th>Наименование</th>
 <th>Единица</th>
 <th>Кол-во</th>
 <th>Цена</th>
 <th>Сумма</th>
 </tr>
 $BODY$
 </table>
 <table width="100%" cellspacing="0" cellpadding="5" border="0">
 <tr valign="bottom">
 <td>
 <br><div>Доставил: www.mqlsoft.net</div>
 <br><div>Получил: <b>$USER_FULL_NAME$ - $_EMAIL$</b></div>
 </td>
 <td align="right">
 <h3>Итого: $ORDER_AMOUNT$</h3>
 <?if($ORDER_TAX$)?><h3>Такса за доставку: $ORDER_TAX$</h3><?endif?>
 <?if($ORDER_DISCOUNT$)?><h3>Скидка по дисконту: $ORDER_DISCOUNT$</h3><?endif?>
 <br><h3>Сумма к оплате выбранным способом: $ORDER_TOPAY_CURR$</h3>
 </td>
 </tr>
 </table>
 </body>
</html>