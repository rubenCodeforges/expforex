<div class="col m12 l4">
 <div class="card sticky-action grey lighten-5 medium">
 <div class="center-align"><p><a href="$ENTRY_URL$" target=_blank><img width="100px" alt="$NAME$" src="$THUMB$" class="gphoto" id="$BLOCK_PREF$-gphoto-$ID$"></a></p></div>
 <div class="card-content">
 <div class="center-align"> <div class="price"><?if($PRICE_OLD$)?> <s>$PRICE_OLD$</s> <span class="newprice">$PRICE$</span> <?else?>$PRICE$<?endif?> </div></div>
 <div class="center-align"> <?if($RATING$)?><?$RSTARS$('12','/.s/img/stars/3/12.png','0','float')?><?endif?></div>
 <span class="card-title activator"><i class="material-icons right">more_vert</i></span>
 <a href="$ENTRY_URL$" target=_blank> <?if(len($NAME$)>60)?><?substr($NAME$,0,60)?>...<?else?>$NAME$<?endif?></a>
 </div>
 <div class="card-action center-align">
 <p><a href="$ENTRY_URL$" class="btn">Подробнее/More</a> </p>
 <?if($IS_IN_BASKET$)?><p> <a href="http://www.expforex.com/shop/checkout" class="btn blue" >Уже в корзине</a> <?else?>
 <a href="javascript://" class="btn orange" onclick="add2Basket('$ID$','$BLOCK_PREF$');">Добавить в корзину</a></p><?endif?>
 <!-- <span class="wishlist" >$2WISHLIST$</span>-->
 </div>
 <div class="card-reveal">
 <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
 <p><b>$NAME$</b></p>
 <p><?if(len($BRIEF$)>120)?><?substr($BRIEF$,0,120)?>...<?else?>$BRIEF$<?endif?></p>
 <p>Категория: <a href="$CAT_URL$">$CAT_NAME$</a></p>
 </div>
 </div>
</div>