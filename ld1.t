$GLOBAL_AHEADER$

<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 $GLOBAL_MENU$
 <!-- ============= Тело ==================-->
 <div class="col m12 l9 ">
 <div style="text-align:center;"> В разделе материалов: <b>$NUM_ENTRIES$</b></div>
 <br>
 <div style="text-align:center;">$PAGE_SELECTOR1$</div>
 <br>
 <div class="row">
 $BODY$
 </div>
 <div style="text-align:center;">$PAGE_SELECTOR1$</div>
 </div>
 </div>
</div>



<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$