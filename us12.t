
<?if($PAGE_ID$ == 'register')?>
<div class="uf-reg-wrap">
	<h2>Регистрация</h2>
	<form id="uf-register" method="post" autocomplete="off" action="">
	<?if($SOCIAL_LOGIN$ && ($ONLY1STEP$ || $STEP$==1))?>
	<div class="uf-soc-icons">
	<p class="uf-soc-openselect">Через социальную сеть</p>
	<p class="uf-soc-closeselect">Выберите социальную сеть...</p>
	<ul>
	<?if($LOGIN_UID$)?><li>$LOGIN_UID$</li><?endif?>
	<?if($LOGIN_FB$)?><li>$LOGIN_FB$</li><?endif?>
	<?if($LOGIN_VK$)?><li>$LOGIN_VK$</li><?endif?>
	<?if($LOGIN_GOOGLE$)?><li>$LOGIN_GOOGLE$</li><?endif?>
	<?if($LOGIN_OK$)?><li>$LOGIN_OK$</li><?endif?>
	<?if($LOGIN_YANDEX$)?><li>$LOGIN_YANDEX$</li><?endif?>
	<?if($LOGIN_TWITTER$)?><li>$LOGIN_TWITTER$</li><?endif?>
	</ul>
	<div class="clear"></div>
	</div>
	<?endif?>

	<div class="uf-fields-wrap">
	<?if($SOCIAL_LOGIN$ && ($ONLY1STEP$ || $STEP$==1))?><p>или через форму на сайте</p><?endif?>
 	<?if($EMAIL$)?>
	<div class="uf-field">
	<label>Email</label>$EMAIL$
	<span class="uf-status-icon text-field" id="uf-email-status-icon"><span class="uf-status"><i></i><p id="uf-email-status"></p></span></span>

	</div>
	<?endif?>

	<?if($PASSWORD$)?>
	<div class="uf-field">
	<label>Пароль</label>$PASSWORD$
	<span class="uf-status-icon text-field" id="uf-password-status-icon"><span class="uf-status"><i></i><p id="uf-password-status"></p></span></span>
	<input type="checkbox" id="uf-show-pass" onchange="document.getElementById('uf-password').type = this.checked ? 'text' : 'password'">
	<label for="uf-show-pass"><span id="uf-show-pass-icon"></span></label>
 	</div>
	<?endif?>

	<?if($NAME$)?>
	<div class="uf-field">
	<label>Имя</label>$NAME$
	<span class="uf-status-icon text-field" id="uf-name-status-icon"></span>
	</div>
	<?endif?>

	<?if($SURNAME$)?>
	<div class="uf-field">
	<label>Фамилия</label>$SURNAME$
	<span class="uf-status-icon text-field" id="uf-surname-status-icon"></span>
	</div>
	<?endif?>

	<?if($NICK$)?>
	<div class="uf-field">
	<label>Никнейм</label>$NICK$
	<span class="uf-status-icon text-field" id="uf-nick-status-icon"><span class="uf-status"><i></i><p id="uf-nick-status"></p></span></span>
	</div>
	<?endif?>

	<?if($BIRTHDAY$)?>
	<div class="uf-field">
	<label>Дата рождения</label>$BIRTHDAY$
	<span class="uf-status-icon" id="uf-birthday-status-icon"></span>
	</div>
	<?endif?>

	<?if($GENDER$)?>
	<div class="uf-field">
	<label>Пол</label>$GENDER$
	<span class="uf-status-icon" id="uf-gender-status-icon"></span>
	</div>
	<?endif?>

	<?if($LOCATION$)?>
	<div class="uf-field">
	<label>Место проживания</label>$LOCATION$
	<span class="uf-status-icon" id="uf-location-status-icon"></span>
	</div>
	<?endif?>

	<?if($CAPTCHA$)?>
	<div class="uf-field">$CAPTCHA$
	<span class="uf-status-icon" id="uf-captcha-status-icon"><span class="uf-status"><i></i><p id="uf-captcha-status"></p></span></span>
	</div>
	<?endif?>

	 <?if($POLICY$)?>
	 <div class="policy">
	 $POLICY$
	 </div>
	 <?endif?>

	 <?if($AGREEMENT$)?>
	 <br>
	 <div class="agreement">
	 $AGREEMENT$
	 </div>
	 <?endif?>

	<?if($TERMS$)?>
	<div class="uf-field">
	$TERMS$
	<span class="uf-status-icon" id="uf-terms-status-icon"></span>
 	</div>
	<?endif?>

	<div id="uf-form-status"></div>$SUBMIT$
	</div>
	</form>
	<div style="clear:both"></div>
</div>

<style type="text/css">
.uf-reg-wrap { text-align: left; max-width: 600px; }
#uf-register .uf-field { margin: 15px 0 20px 0; white-space:nowrap; position:relative; }
#uf-register .uf-field>label { display:block; margin:3px 0; text-align:left }
#uf-register .uf-field input[type=text]:not([name=captcha]):not([name=code]) {width:85%; font-size:1em; padding-left:5px; padding-right:5px; }
#uf-register .uf-field img { vertical-align:middle;}
#uf-register input#uf-show-pass[type="checkbox"]{ display:none }
#uf-register input#uf-show-pass[type="checkbox"] + label span { display:inline-block; width:16px; height:16px; background:url(/.s/img/icon/social/pw.svg) no-repeat center center #c6c6c6; background-size:80% 80%; vertical-align:middle; margin-left:-23px; cursor:pointer; border-radius:10px }
#uf-register input#uf-show-pass[type="checkbox"] + label { display:inline-block }
#uf-register input#uf-show-pass[type="checkbox"]:checked + label span { background:url(/.s/img/icon/social/pw-s.svg) no-repeat center center #c6c6c6; background-size:80% 80% }
#uf-register .uf-changed input#uf-show-pass[type="checkbox"] + label span { margin-left:-38px;}
#uf-register .uf-status-icon { display:none; vertical-align:middle; width:16px; height:16px; background-repeat:no-repeat; background-size:cover; position:relative }
#uf-register .uf-status-icon.text-field { margin-left:-23px }
#uf-captcha-status-icon.uf-status-icon{position:absolute; left:105px; top:13px; }
#uf-captcha-img { width:125px; height:35px }
#uf-email-status-icon.fail, #uf-password-status-icon.fail, #uf-captcha-status-icon.fail { cursor:pointer }
#uf-register span.fail { display:inline-block; background-image:url(/.s/img/icon/social/fail.svg) }
#uf-register span.pass { display:inline-block; background-image:url(/.s/img/icon/social/pass.svg) }
#uf-register span.wait { display:inline-block; background-image:url(/.s/img/icon/ajsml.gif) }
#uf-register span.uf-status{ display:none; position:absolute; right:-15px; margin:0; z-index:5 }
#uf-register span.uf-status p { background-color:rgba(0,0,0,0.7); color:#fff; padding:5px 10px; white-space:nowrap; font:12px/29px 'Arial'; border-radius:3px; margin:6px 0 0 0;  }
#uf-register span.uf-status i { float:right; margin-right:18px;  display:block;  width:0; height:0; border-left:6px solid transparent; border-right:6px solid transparent; border-bottom:6px solid rgba(0,0,0,0.7) }
#uf-register .uf-refresh { cursor:pointer }
#uf-captcha { width:80px!important }
#uf-register .captcha img { vertical-align:middle }
#uf-submit { margin:10px 0 10px 0; clear:both }
#uf-submit.disabled { opacity:0.2 }
#uf-register .uf-field input.uf-gender, #uf-register .uf-field input.uf-click { width:auto }
#uf-register .uf-field label.uf-inline-label, #uf-register .uf-field label#uf-terms-label { display:inline }
#uf-register .uf-fields-wrap, #uf-register .uf-soc-icons{ float:left; width:50%; padding:1px 0 }
#uf-register .uf-soc-closeselect { display:none }
.uf-soc-icons ul { list-style-type:none; margin:0; padding:0 }
.uf-soc-icons ul li { text-align:left; float:left; width:50%; margin:2px 0 }
#uf-register .uf-soc-icons ul li b { display:block; position:absolute; left:0; padding-left:42px; top:8px; width:60px; font-weight:normal }

@media screen and (max-width:768px) {
	.uf-reg-wrap { text-align:center }
	a.login-with i { width:44px; height:44px; border-radius:24px;}
	#uf-register .uf-fields-wrap, #uf-register .uf-soc-icons { float:none; width:100%;text-align:center }
	#uf-register .uf-reg-wrap, #uf-register .uf-field label, #uf-register .uf-field { text-align:center }
	#uf-register .uf-soc-icons ul { margin:0 auto; overflow:auto; display:inline-block }
	#uf-register .uf-soc-icons ul li { width:auto; margin:5px }
	#uf-register .uf-soc-icons a.login-with b { display:none }
	#uf-register .uf-soc-closeselect { display:block }
	#uf-register .uf-soc-openselect { display:none }
	#uf-register .uf-field input { padding:10px 4%; font-size:120%; margin:0 auto }
	#uf-submit { margin:0 auto }
	#uf-register input#uf-show-pass[type="checkbox"] + label span { margin-left:-38px }
	#uf-register .uf-changed input#uf-show-pass[type="checkbox"] + label span { margin-left:-60px;}
}
</style>
$JS_VALIDATOR$
<?endif?>

<?if($PAGE_ID$ == 'verify')?>
	<h2>Отлично!</h2>
	<h3>Теперь необходимо подтвердить e-mail адрес</h3>
	<div style="text-align:center; padding:20px;">На <b>$EMAIL$</b> было отправлено письмо с ссылкой для подтверждения регистрации.<br>Для завершения регистрации перейдите по ссылке из полученного письма.</div>
<?endif?>

<?if($PAGE_ID$ == 'confirm')?>
<h2>Подтверждение e-mail адреса</h2>
<div style="text-align:center; padding:20px;">
	<?if($CONFIRM_ERROR$)?>
		$CONFIRM_ERROR$
	<?else?>
		Поздравляем! вы успешно подтвердили e-mail адрес
	<?endif?>
	<br><br><a href="/">На главную</a>
</div>
<?endif?>
		