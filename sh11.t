<?if($BODY$)?>

<ul class="collection">$BODY$</ul>

<div class="center align">
 <p> Ваша корзина</p>
 <p><a class="btn" href="javascript://" rel="nofollow" onclick="return clearBasket();">Очистить</a></p>
 <p><a class="btn" href="/shop/checkout">Оформить заказ $ORDER_AMOUNT$</a></p>
 
</div>
<?else?>
<div class="center align">Ваша корзина пуста</div>
<?endif?>


<script language="Javascript">
 var lock_buttons = 0;
 
 function clearBasket(){
 if(lock_buttons) return false; else lock_buttons = 1;
 var el = $('#shop-basket');
 if(el.length){ var g=document.createElement("div"); $(g).addClass('myWinGrid').attr("id",'shop-basket-fade').css({"left":"0","top":"0","position":"absolute","border":"#CCCCCC 1px solid","width":$(el).width()+'px',"height":$(el).height()+'px',"z-index":5}).hide().bind('mousedown',function(e){e.stopPropagation();e.preventDefault();_uWnd.globalmousedown();}).html('<div class="myWinLoad" style="margin:5px;"></div>'); $(el).append(g); $(g).show(); }
 _uPostForm('',{type:'POST',url:'/shop/basket',data:{'mode':'clear'}});
 ga_event('basket_clear');
 return false;
 }
 
 function removeBasket(id){
 if(lock_buttons) return false; else lock_buttons = 1;
 $('#basket-item .with-clear-'+id+' .sb-func').removeClass('remove').addClass('myWinLoadS').attr('title','');
 _uPostForm('',{type:'POST',url:'/shop/basket',data:{'mode':'del', 'id':id}});
 return false;
 }
 
 function add2Basket(id,pref){
 if(lock_buttons) return false; else lock_buttons = 1;
 var opt = new Array();
 $('#b'+pref+'-'+id+'-basket').attr('disabled','disabled');
 $('#'+pref+'-'+id+'-basket').removeClass('done').removeClass('err').removeClass('add').addClass('wait').attr('title','');
 $('#'+pref+'-'+id+'-options-selectors').find('input:checked, select').each(function(){ opt.push(this.id.split('-')[3]+(parseInt(this.value) ? '-'+this.value :''));});
 _uPostForm('',{type:'POST',url:'/shop/basket',data:{'mode':'add', 'id':id, 'pref':pref, 'opt':opt.join(':'), 'cnt':$('#q'+pref+'-'+id+'-basket').attr('value')}});
 ga_event('basket_add');
 return false;
 }
 
 function buyNow(id,pref){
 if(lock_buttons) return false; else lock_buttons = 1;
 var opt = new Array();
 $('#b'+pref+'-'+id+'-buynow').attr('disabled','disabled');
 $('#'+pref+'-'+id+'-buynow').removeClass('done').removeClass('err').removeClass('now').addClass('wait').attr('title','');
 $('#'+pref+'-'+id+'-options-selectors').find('input:checked, select').each(function(){ opt.push(this.id.split('-')[3]+(parseInt(this.value) ? '-'+this.value :''));});
 _uPostForm('',{type:'POST',url:'/shop/basket',data:{'mode':'add', 'id':id, 'pref':pref, 'opt':opt.join(':'), 'cnt':$('#q'+pref+'-'+id+'-basket').attr('value'), 'now':1}});
 ga_event('basket_buynow')
 return false;
 }
 jQuery('#shop-basket a').on('click touchend', function(e) {
 var el = jQuery(this);
 var link = el.attr('href');
 window.location = link;
 });
</script>