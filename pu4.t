$GLOBAL_AHEADER$

<!-- ====================================================================================================== -->
<!-- Скрипты -->
<!-- ====================================================================================================== -->

<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->

<div class="container">
 <div class="row">
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($SHOP_PATH$)?> <a href="http://www.expforex.com/shop" class="breadcrumb"> MQL Market</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 
 <div class="col m12 l3"> 
 <div class="col s12 center-align">
 <br> <a href="$ENTRY_URL$"><?if($SCREEN_URL$)?> <img class="responsive-img" src="$SCREEN_URL$"><?else?><?if($IMG_URL1$)?> <img class="responsive-img" src="$IMG_URL1$"><?else?><img class="responsive-img" src="/1NEWDESIGN/NEWIMAGES/NOIMAGE.jpg"><?endif?><?endif?></a> 
 <br> 
 </div>
 <!-- ====================================================================================================== -->
 <!-- Боковая панель справа START -->
 <!-- ====================================================================================================== -->
 <div class="col s12 z-depth-3">
 <br>
 
 <i class="material-icons left">date_range</i> <span title="$TIME$">Добавлено: $ADD_DATE$ </span> <br> <br>
 <i class="material-icons left">date_range</i> <span title="$TIME$">Обновлено:
 <script type="text/javascript"> 
 var yearX, monthX, dayX = 0; 
 a = $LASTMOD$; 
 b = parseInt(a/86400); 
 c = b-730; 
 d = parseInt(c/1461); 
 yearX = 1972+(d*4); 
 e = c-(d*1461); 
 if(e>366){e=e-366; yearX=yearX+1}; 
 if(e>365){while (e>365){e=e-365; yearX=yearX+1}}; 
 k=e; 
 monthX=1; 
 if(yearX%4==0){l=60}else{l=59}; 
 if(k>l){monthX=monthX+2; k=k-l}else{if(k>31){k=k-31; monthX=monthX+1};}; 
 if(k>61){while(k>61){k=k-61; monthX=monthX+2}}; 
 if(k>31){k=k-31; monthX=monthX+1}; 
 dayX=k+1; 
 if(dayX<10){dayX='0' + dayX}; 
 if(monthX<10){monthX='0' + monthX}; 
 document.write(dayX + '.' + monthX + '.' + yearX); 
 </script>
 
 
 </span> <br> <br>
 <?if($FILE_URL$)?> <i class="material-icons left">attach_file</i> Загрузок: $LOADS$<br><br><?else?><i class="material-icons left">remove_red_eye</i>Просмотров: $READS$<br><br><?endif?> 
 <?if($CAT_NAME$)?> <a href="$CAT_URL$"><i class="material-icons left">dashboard</i>Категория: $CAT_NAME$</a> <br><br><?endif?> 
 <?if($SOURCE$)?> <a href="$SOURCE$"><i class="material-icons left">surround_sound</i>Источник</a> <br><br><?endif?> 
 
 
 <?if($OTHER7$)?> <i class="material-icons left">merge_type</i>Тип: $OTHER7$ <br><br><?endif?> 
 <?if($OTHER8$)?> <i class="material-icons left">open_with</i>Особенности: $OTHER8$ <br><br><?endif?> 
 <?if($OTHER9$)?> <i class="material-icons left">computer</i>Терминал: $OTHER9$ <br><br><?endif?> 
 
 <?if($TAGS$)?> <i class="material-icons left">cloud</i>$TAGS$<br><br><?endif?> 
 
 
 <div class="col s12 center-align">
 <br>
 <?if($OTHER3$)?> <a class="btn" href="$OTHER3$"><i class="material-icons left">forum</i>Обсуждение</a> <br><br><?endif?>
 
 <?ifnot($FILE_URL$)?>
 <?if($OTHER2$ || $OTHER4$ || $OTHER12$)?> 
 <p class="center-align"><b>Скачать файл</b></p>
 <?if($OTHER2$)?><?ifnot($FILE_URL$)?><a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Скачать бесплатно с нашего сайта" href="$OTHER2$"><i class="material-icons left">file_download</i>Скачать для МТ4</a><?endif?> <br><br><?endif?> 
 <?if($OTHER4$)?><?ifnot($FILE_URL$)?><a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Скачать бесплатно с нашего сайта" href="$OTHER4$"><i class="material-icons left">file_download</i>Скачать для МТ5</a><?endif?> <br><br><?endif?> 
 <?if($OTHER12$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Скачать бесплатно с маркета mql5.com" href="$OTHER12$"><i class="material-icons left">file_download</i>Скачать с mql5.com</a> <br><br><?endif?> 
 <?endif?> 
 <?endif?> 
 
 
 
 <?if($OTHER5$ || $OTHER5$)?> 
 <p class="center-align"><b>Купить лицензию с mql5</b></p>
 <?if($OTHER5$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить лицензию или арендовать на 10 компьютеров" href="$OTHER5$"><i class="material-icons left">shopping_cart</i>Купить для МТ4</a> <br><br><?endif?> 
 <?if($OTHER5$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить лицензию или арендовать на 10 компьютеров" href="$OTHER10$"><i class="material-icons left">shopping_cart</i>Купить для МТ5</a> <br><br><?endif?> 
 <?endif?> 
 
 <?if($OTHER6$ || $OTHER11$)?> 
 <p class="center-align"><b>Купить Безлимит</b></p>
 <?if($OTHER6$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить версию без ограничений" href="$OTHER6$"><i class="material-icons left">add_shopping_cart</i>Купить для МТ4</a> <br><br><?endif?> 
 <?if($OTHER11$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить версию без ограничений" href="$OTHER11$"><i class="material-icons left">add_shopping_cart</i>Купить для МТ5</a> <br><br><?endif?> 
 <?endif?> 
 
 <?if($OTHER13$ || $OTHER14$)?> 
 <p class="center-align"><b>Купить Открытый код</b></p>
 <?if($OTHER13$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить открытый код этого советника" href="$OTHER13$"><i class="material-icons left">lock_open</i>Купить для МТ4</a> <br><br><?endif?> 
 <?if($OTHER14$)?> <a class="btn tooltipped" data-position="top" data-delay="50" data-tooltip="Купить открытый код этого советника" href="$OTHER14$"><i class="material-icons left">lock_open</i>Купить для МТ5</a> <br><br><?endif?>
 <?endif?> 
 
 <br><br>$ADVBT_1$<br>
 </div>
 </div>
 
 
 </div>
 
 <!-- ====================================================================================================== -->
 <!-- Боковая панель справа END -->
 <!-- ====================================================================================================== -->
 <!-- ====================================================================================================== -->
 <!-- Центральная панель с информацией START -->
 <!-- ====================================================================================================== -->
 <div class="col m12 l9 "> 
 <div class="col m12 center-align"> 
 <?if($MODER_PANEL$)?>$MODER_PANEL$<?endif?> <br>
 <h2 class="flow-text">$ENTRY_TITLE$</h2>
 <?if($FILE_URL$)?><a class="btn-large" href="$FILE_URL$"><i class="material-icons left">file_download</i>Скачать $FILE_SIZE$</a><?endif?>
 <br><br>
 </div>
 
 <ul class="tabs" >
 <li class="tab col s4"><a href="#test1">Описание</a></li>
 <?if($IMG_URL1$)?><li class="tab col s4"><a href="#test2">Скриншоты</a></li><?endif?> 
 <!-- <?if($FILE_URL$)?><li class="tab col s4"><a href="#test4">История версий</a></li><?endif?> -->
 </ul>
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Вкладка описание -->
 <!-- ====================================================================================================== --> 
 <div id="test1" class="col s12"> 
 <!-- ====================================================================================================== -->
 <!-- Показываем видео -->
 <!-- ====================================================================================================== -->
 
 <?if($OTHER1$)?>
 <div class="center-align"> 
 <br>
 <div class="video-container">
 <iframe width="640px" src="//www.youtube.com/embed/$OTHER1$?rel=0" frameborder="0" allowfullscreen></iframe>
 </div>
 <br>
 </div>
 <?endif?> 
 <!-- ====================================================================================================== --> 
 <!-- Оглавление-->
 <!-- ====================================================================================================== --> 
 <?if($MODULE_ID$=="publ")?>
 <div class="tablecontents" style="bottom: 45px; "></div>
 <?endif?>
 <!-- ====================================================================================================== --> 
 <h2>Описание</h2>
 <p align=left><article>$MESSAGE$</article></p> 
 <p align=center> <?if($OTHER2$)?><?if($FILE_URL$)?><a class="btn-large" href="$OTHER2$"> <i class="material-icons left">fullscreen</i>Полное описание</a><?endif?><?endif?> </p> 
 </div> 
 <!-- ====================================================================================================== -->
 <!-- Вкладка СкриншотыПоказываем картинки -->
 <!-- ====================================================================================================== -->
 <?if($IMG_URL1$)?>
 <div id="test2" class="col s12"> 
 <br>
 <div class="center-align">
 <a id="prev" class="btn"><i class="material-icons left">keyboard_arrow_left</i>Prev</a>
 <a id="next" class="btn"><i class="material-icons right">keyboard_arrow_right</i>Next</a>
 </div>
 <br>
 
 <div class="carousel carousel-slider">
 <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL50$"></a>
 <?if($IMG_URL1$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL1$"></a><?endif?>
 <?if($IMG_URL2$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL2$"></a><?endif?> 
 <?if($IMG_URL3$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL3$"></a><?endif?> 
 <?if($IMG_URL4$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL4$"></a><?endif?> 
 <?if($IMG_URL5$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL5$"></a><?endif?> 
 <?if($IMG_URL6$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL6$"></a><?endif?> 
 <?if($IMG_URL7$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL7$"></a><?endif?> 
 <?if($IMG_URL8$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL8$"></a><?endif?> 
 <?if($IMG_URL9$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL9$"></a><?endif?> 
 <?if($IMG_URL10$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL10$"></a><?endif?> 
 <?if($IMG_URL11$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL11$"></a><?endif?> 
 <?if($IMG_URL12$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL12$"></a><?endif?> 
 <?if($IMG_URL13$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL13$"></a><?endif?> 
 <?if($IMG_URL14$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL14$"></a><?endif?> 
 <?if($IMG_URL15$)?> <a class="carousel-item"><img class="responsive-img" src="http://www.expforex.com$IMG_URL15$"></a><?endif?> 
 
 </div>
 
 </div>
 <?endif?> 
 
 
 
 
 </div>
 </div>
 
 
 <!-- ====================================================================================================== -->
 <!-- Центральня панель END -->
 
 <!-- ====================================================================================================== -->
 <!-- Комментарии START -->
 <!-- ====================================================================================================== --> 
 
 <?if($COM_IS_ACTIVE$)?>
 <?if($COM_CAN_READ$)?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr><td width="60%" height="25"><!--<s5183>-->Всего комментариев<!--</s>-->: <b>$COM_NUM_ENTRIES$</b></td><td align="right" height="25">$COM_PAGE_SELECTOR$</td></tr>
 <tr><td colspan="2">$COM_BODY$</td></tr>
 <tr><td colspan="2" align="center">$COM_PAGE_SELECTOR1$</td></tr>
 <tr><td colspan="2" height="10"></td></tr>
 </table>
 <?endif?>
 
 <?if($COM_CAN_ADD$)?>
 $COM_ADD_FORM$
 <?else?>
 <?if($USER_LOGGED_IN$)?><?else?><div align="center" class="commReg"><!--<s5237>-->Добавлять отзывы могут только зарегистрированные пользователи.<!--</s>--><br />[ <a href="$REGISTER_LINK$"><!--<s3089>-->Регистрация<!--</s>--></a> | <a href="$LOGIN_LINK$"><!--<s3087>-->Вход<!--</s>--></a> ]</div><?endif?>
 <?endif?>
 <?endif?>
 
 
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Похожие записи START -->
 <!-- ====================================================================================================== -->
 
 <div class="row hide-on-small-only">
 <div class="col s12">
 <div class="section">
 <h5>Похожие записи</h5>
 <div class="collection"><?$RELATED_ENTRIES$(10)?></div>
 </div>
 
 
 <font style="font-size: 1pt; "> Здесь можно <a href="$ENTRY_URL$">скачать $ENTRY_TITLE$</a><br>
 <a href="$ENTRY_URL$">Купить $ENTRY_TITLE$</a> можно здесь - 
 <a href="$ENTRY_URL$">Прочитать о $ENTRY_TITLE$</a> можно здесь - 
 <a href="$ENTRY_URL$">Обсуждение $ENTRY_TITLE$</a> - стейты, отзывы, комментарии - 
 <a href="$ENTRY_URL$">Отзывы о $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Настройки к $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Мониторинг $ENTRY_TITLE$ </a> - 
 
 <a href="$ENTRY_URL$">Download $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Buy $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Read about $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Discusion of $ENTRY_TITLE$</a> - 
 <a href="$ENTRY_URL$">Comments of $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Settings $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">Monitoring $ENTRY_TITLE$ </a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ скачать бесплатно</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ ключ</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ открытый код</a> - 
 <a href="$ENTRY_URL$">$ENTRY_TITLE$ скачать</a></font> -
 
 
 
 </div>
 </div>
 <!-- ====================================================================================================== -->
 <!-- Похожие записи END -->
 <!-- ====================================================================================================== -->
 
 
</div>



<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
<script>
 var ToC ="<h2>Оглавление</h2>"+
 "<ul class='section table-of-contents'>";
 var newLine, el, title, link;
 $("article a").each(function() {
 el = $(this);
 
 if($(this).attr("name"))
 {
 title = el.text();
 link = "#" + el.attr("name");
 newLine = "<li>" +"<i class='material-icons left'>play_arrow</i>"+"<a href='" + link + "'>" +title +"</a>" + "</li>";
 ToC += newLine;
 }
 });
 ToC +=
 "</ul>" ; 
 $(".tablecontents").prepend(ToC);
</script>


<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$