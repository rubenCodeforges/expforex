$GLOBAL_AHEADER$

<!-- ====================================================================================================== -->
<!-- Тело страницы START -->
<!-- ====================================================================================================== -->


<div class="container">
 <div class="row">
 <!-- ============= Путь ==================-->
 <div class="col s12 hide-on-med-and-down">
 <nav style="background-color:#1976d2">
 <div class="nav-wrapper">
 <div class="col s12">
 <?if($SITE_URL$)?><a href="$SITE_URL$" class="breadcrumb">Главная</a><?endif?>
 <?if($MODULE_URL$)?><a href="$MODULE_URL$" class="breadcrumb"> $MODULE_NAME$</a><?endif?>
 <?if($SECTION_URL$)?> <a href="$SECTION_URL$" class="breadcrumb"> $SECTION_NAME$</a><?endif?>
 <?if($SHOP_PATH$)?> <a href="http://www.expforex.com/shop" class="breadcrumb"> MQL Market</a><?endif?>
 <?if($CAT_URL$)?> <a href="$CAT_URL$" class="breadcrumb"> $CAT_NAME$</a><?endif?>
 
 </div> 
 </div>
 </nav>
 </div>
 <div class="col s12"><br></div>
 <!-- ============= Меню раздела ==================-->
 
 <!-- ====================================================================================================== -->
 <!-- Начало 2 колонок тела -->
 <!-- ====================================================================================================== --> 
 <div class="col m12 ">
 
 <!-- <body> -->
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Центральное название товара -->
 <!-- ====================================================================================================== -->
 
 
 <div class="col s12 center-align">
 
 </div>
 
 
 <!-- ====================================================================================================== -->
 <!-- Левая часть товара -->
 <!-- ====================================================================================================== -->
 <div class="col s12 m4">
 
 
 <div class="center-align">
 <img alt="$ENTRY_TITLE$" width="50%" src="$PHOTO$" onclick="_bldCont1($ID$, this.getAttribute('idx'));" id="ipreview" idx="0" title="Кликните для увеличения изображения">
 $IMGS_ARRAY_JS$
 </div>
 <div class="divider"></div> 
 <div class="">
 <br>
 <?if($RATING$)?>
 <div class="center-align"> 
 <?$RSTARS$('30','/.s/img/stars/3/30.png','1','float')?>
 </div>
 <?endif?>
 <p> <i class="material-icons left">attach_money</i>Цена: <b><?if($PRICE_OLD$)?><s>$PRICE_OLD$</s> <span class="newprice">$PRICE$</span><?else?>$PRICE$<?endif?> </b></p>
 <p> <i class="material-icons left">star</i>Рейтинг: <b><span id="entRating$ID$">$RATING$</span></b>/<span id="entRated$ID$">$RATED$</span></p>
 <p> <i class="material-icons left">date_range</i> <span title="$TIME$">Добавлено: <b>$ADD_DATE$</b> </span> </p>
 <p> <i class="material-icons left">date_range</i> <span title="$TIME$">Обновлено: <b>$MOD_DATE$</b></span> </p> 
 <p> <i class="material-icons left">check</i> Продано: <b>$ENTRY_SOLDS$</b></p>
 <p><?if($ENTRY_TYPE$ == 1)?><i class="material-icons left">photo_size_select_small</i>Размер: <b>$FILE_SIZE$</b><?endif?></p>
 <p> <?if($OTHER3$)?><i class="material-icons left">computer</i>Терминал: <b>$OTHER3$</b><?endif?></p>
 <p> <?if($OTHER4$)?><i class="material-icons left">games</i>Особенности: <b>$OTHER4$</b><?endif?></p>
 <p> <?if($TAGS$)?><i class="material-icons left">cloud</i>Теги: <b>$TAGS$</b><?endif?></p>
 
 </div> 
 </div> 
 
 
 <!-- ====================================================================================================== -->
 <!-- Правая часть товара -->
 <!-- ====================================================================================================== -->
 
 <div class="col s12 m8"> 
 <?if($MODER_PANEL$)?>$MODER_PANEL$ <?endif?>
 <h1>$ENTRY_TITLE$</h1>
 
 <!-- ====================================================================================================== -->
 <!-- Блок с кнопками купить -->
 <!-- ====================================================================================================== --> 
 <div class="col s12 m6 center-align">
 
 <?if($BUY_NOW$)?>
 <p><a href="javascript://" class="btn" onclick="buyNow('$ID$','$BLOCK_PREF$');">Купить сейчас / Buy now $PRICE$</a></p>
 <p><?if($IS_IN_BASKET$)?> <a href="http://www.expforex.com/shop/checkout" class="btn" >Уже в корзине / Always in basket</a> 
 <?else?>
 <a href="javascript://" class="btn btn-warning" onclick="add2Basket('$ID$','$BLOCK_PREF$');">Добавить в корзину / Add to basket</a>
 <?endif?>
 <p class="center-align"><a href="http://www.expforex.com/index/kak_kupit_sovetnik_v_nashem_magazine/0-251"> Как Оплатить?</a> - 
 <a href="http://www.expforex.com/shop/10/desc/test-dlja-proby-pokupki-tovara"> Пробная покупка 1$</a> - 
 <a href="http://www.expforex.com/index/internet_magazin_uslovija_i_pomoshh/0-250"> Условия покупки </a>
 </p> 
 <div class="wishlist" align ="center">$2WISHLIST$</div> 
 </div>
 <?endif?> 
 
 
 
 
 
 <div class="col s12 m6 center-align"> 
 <p class="center-align"> <?if($OTHER5$)?> <a href="$OTHER5$" class="btn blue" target="_blank">Скачать инструкцию PDF</a> <?endif?></p>
 <p class="center-align"> <?if($OTHER6$)?> <a href="$OTHER6$" class="btn blue" target="_blank">Скачать DEMO версию</a> <?endif?></p>
 <p class="center-align"> <?if($OTHER8$)?> <a href="$OTHER8$" class="btn blue" target="_blank">Купить на mql5.com</a> <?endif?></p>
 <p class="center-align"> <?if($OTHER9$)?> <a href="$OTHER9$" class="btn blue" target="_blank">Обсуждение на форуме</a> <?endif?></p>
 </div>
 </div> 
 
 
 </div>
 
 
 <!-- ====================================================================================================== -->
 <!-- Правая часть товара -->
 <!-- ====================================================================================================== -->
 
 <div class="col s12"> 
 <br>
 <ul class="tabs" >
 <li class="tab col s3"><a href="#test1">Описание</a></li>
 <li class="tab col s3"><a href="#test2">Изображения</a></li>
 <li class="tab col s3"><a href="#test3">Комментарии($COM_NUM_ENTRIES$)</a></li>
 <li class="tab col s3"><a href="#test4">Информация</a></li>
 </ul> 
 
 </div> 
 
 
 
 <!-- ====================================================================================================== -->
 <!-- Конец тела -->
 <!-- ====================================================================================================== --> 
 </div>
 
 <!-- ====================================================================================================== -->
 <!-- Вкладка описание -->
 <!-- ====================================================================================================== --> 
 <div id="test1" class="col s12"> 
 <!-- ====================================================================================================== -->
 <!-- Показываем видео -->
 <!-- ====================================================================================================== -->
 
 <div class="col s12 m6"> 
 <p align=left><?if($DESCRIPTION$)?>$DESCRIPTION$<?else?><!--<s4782>-->Описание товара отсутствует<!--</s>--><?endif?></p> 
 <p align=center> <?if($OTHER2$)?><a class="btn" href="$OTHER2$" target="_blank"> <i class="material-icons left">fullscreen</i>Полное описание и инструкция</a><?endif?> </p> 
 </div>
 
 <?if($OTHER1$)?>
 <h3>Видео</h3>
 <div class="col s12 m6 center-align"> 
 
 <br>
 <div class="video-container">
 <iframe width="640px" src="//www.youtube.com/embed/$OTHER1$?rel=0" frameborder="0" allowfullscreen></iframe>
 </div>
 <br>
 </div>
 <?endif?> 
 
 </div> 
 
 <!-- ====================================================================================================== -->
 <!-- Вкладка изображения -->
 <!-- ====================================================================================================== --> 
 <div id="test2" class="col s12"> 
 <?if($PHOTO$)?><img alt="" src="$THUMB$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="0" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_1$)?><img alt="" src="$THUMB_1$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="1" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_2$)?><img alt="" src="$THUMB_2$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="2" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_3$)?><img alt="" src="$THUMB_3$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="3" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_4$)?><img alt="" src="$THUMB_4$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="4" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_5$)?><img alt="" src="$THUMB_5$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="5" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_6$)?><img alt="" src="$THUMB_6$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="6" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_7$)?><img alt="" src="$THUMB_7$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="7" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_8$)?><img alt="" src="$THUMB_8$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="8" title="Кликните для увеличения изображения"><?endif?>
 <?if($PHOTO_9$)?><img alt="" src="$THUMB_9$" class="gphoto" onclick="_bldCont1($ID$, this.getAttribute('idx'));" idx="9" title="Кликните для увеличения изображения"><?endif?>
 </div> 
 
 <!-- ====================================================================================================== -->
 <!-- Вкладка комментарии -->
 <!-- ====================================================================================================== --> 
 <div id="test3" class="col s12"> 
 <?if($COM_IS_ACTIVE$)?>
 <?if($COM_CAN_READ$)?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr><td width="60%" height="25"><!--<s5183>-->Всего комментариев<!--</s>-->: <b>$COM_NUM_ENTRIES$</b></td><td align="right" height="25">$COM_PAGE_SELECTOR$</td></tr>
 <tr><td colspan="2">$COM_BODY$</td></tr>
 <tr><td colspan="2" align="center">$COM_PAGE_SELECTOR1$</td></tr>
 <tr><td colspan="2" height="10"></td></tr>
 </table>
 <?endif?>
 
 <?if($COM_CAN_ADD$)?>
 $COM_ADD_FORM$
 <?else?>
 <?if($USER_LOGGED_IN$)?><?else?><div align="center" class="commReg"><!--<s5237>-->Добавлять комментарии могут только зарегистрированные пользователи.<!--</s>--><br />[ <a href="$REGISTER_LINK$"><!--<s3089>-->Регистрация<!--</s>--></a> | <a href="$LOGIN_LINK$"><!--<s3087>-->Вход<!--</s>--></a> ]</div><?endif?>
 <?endif?>
 <?endif?>
 
 </div> 
 
 
 <!-- ====================================================================================================== -->
 <!-- Вкладка информация -->
 <!-- ====================================================================================================== --> 
 <div id="test4" class="col s12"> 
 
 <p><span>Внимание!</span></p>
 <p><span>Демо версия / Мониторинг предназначен для проверки данного продукта. Покупая данный продукт, Вы соглашаетесь,что поняли предназначение системы и Вам подходит данный продукт! Вы даете согласие, что приобретаемый продукт был протестирован Вами в демо версии. ВЫ просмотрели мониторинг и согласны с ним. Денежные средства не возвращаются.</span></p>
 <p><span>Демо версия не предназначена для использования на реальном счету.</span></p>
 
 <b><span><a class="btn" href="$OTHER6$">Скачать Демо версию $ENTRY_TITLE$</a></span></b>
 
 <p>Сайт и www.expforex.com не несет ответственности за не правильное \ не целевое использование советников и программ.<br />
 Любые рыночные ситуации могут принести вред Вашему счету.<br />
 Перед началом реальной торговли настоятельно рекомендуем изучить учебные материалы, набраться опыта, отточить навыки торговли на демо счетах.</span></p>
 <br />
 <span>Внимательно прочтите&nbsp;<a href="http://www.expforex.com/index/internet_magazin_uslovija_i_pomoshh/0-250">Условия покупки</a>&nbsp;товаров перед совершением оплаты.<br />
 Нажимая кнопку &quot;Оплатить&quot; Вы соглашаетесь с условиями приобретения товара.<br />
 
 
 </div>
 
 
 
 
 <br>
 <div class="divider"></div>
 <?if($RECOMMENDED_PRODUCTS_PRESENT$)?><div class="row"><h3>С этим товаром покупают</h3><?$RECOMMENDED_PRODUCTS$(0)?></div><?endif?>
 
 <?if($LASTV_3$)?><div class="row"><h3>Просмотренные ранее товары</h3>$LASTV_3$</div><?endif?>
 
 
 
 
 
 
 <!-- </body> -->
 </div>
</div>



<script type="text/javascript">
 function _bldCont(indx){
 var bck=indx-1;var nxt=indx+1;
 if (bck<0){bck = allEntImgs$ID$.length-1;}
 if (nxt>=allEntImgs$ID$.length){nxt=0;}
 var imgs='';
 if (allEntImgs$ID$.length>1){
 for (var i=0;i<allEntImgs$ID$.length;i++){var img=i+1;
 if(allEntImgs$ID$[i][0].length<1){continue;}
 if (i==indx){imgs += '<b class="pgSwchA">'+img+'</b> ';}
 else {imgs += '<a class="pgSwch" href="javascript://" rel="nofollow" onclick="_bldCont('+i+');return false;">'+img+'</a> ';}
 }
 imgs = '<div align="center" style="padding:8px 0 5px 0;white-space:nowrap;overflow:auto;overflow-x:auto;overflow-y:hidden;"><a class="pgSwch" href="javascript://" rel="nofollow" onclick="_bldCont('+bck+');return false;">&laquo; Back</a> '+imgs+'<a class="pgSwch" href="javascript://" rel="nofollow" onclick="_bldCont('+nxt+');return false;">Next &raquo;</a> </div> ';}
 var hght = parseInt(allEntImgs$ID$[indx][2]); if ($.browser.msie) { hght += 28; };
 _picsCont = '<div id="_prCont" style="position:relative;"><img alt="" border="0" src="' + allEntImgs$ID$[indx][0] + '"/>'+imgs+'</div>';
 new _uWnd('wnd_prv', "Изображения товара", 10, 10, { waitimages:300000, autosizewidth:1, hideonresize:1, autosize:1, fadetype:1, closeonesc:1, align:'center', min:0, max:0, resize:1 }, _picsCont);
 }
</script>
<!-- ====================================================================================================== -->
<!-- Тело страницы END -->
<!-- ====================================================================================================== -->
$GLOBAL_BFOOTER$