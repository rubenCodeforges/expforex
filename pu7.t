<div class="row">
 <div class="col s12">
 <div class="card-panel z-depth-3">
 <div class="row">
 <div class="col s12 m2 center-align"> 
 <br> <a href="$ENTRY_URL$"><?if($SCREEN_URL$)?> <img class="responsive-img" src="$SCREEN_URL$"><?else?><?if($IMG_URL1$)?> <img class="responsive-img" src="$IMG_URL1$"><?else?><img class="responsive-img" src="/1NEWDESIGN/NEWIMAGES/NOIMAGE.jpg"><?endif?><?endif?></a> 
 </div>
 <div class="col s12 m10 center-align"> 
 <a href="$ENTRY_URL$"><h3 class="flow-text">$TITLE$</h3></a>
 </div>
 
 <div class="divider"></div><br>
 
 <div class="col s12 "> 
 <p align=left>$MESSAGE$</p> 
 </div>
 <br>
 
 
 <div class="col s12 center-align">
 <a class="waves-effect waves-light btn align-center" href="$ENTRY_URL$"><i class="material-icons left">arrow_forward</i> Подробнее</a>
 </div>
 
 </div>
 <div class="divider"></div><br>
 
 <div class="row">
 <div class="col s4 "> 
 <?if($CATEGORY_NAME$)?> <a href="$CATEGORY_URL$"><i class="material-icons left">dashboard</i>$CATEGORY_NAME$</a> <?endif?>
 </div>
 <div class="col s4 "> 
 <i class="material-icons left">date_range</i> <span title="$TIME$"> $DATE$ </span>
 </div>
 
 <div class="col s4 "> 
 <?if($LOADS$)?> <span> <i class="material-icons left">attach_file</i> $LOADS$</span><?else?>
 <span> <i class="material-icons left">remove_red_eye</i> $READS$</span><?endif?> 
 <?if($MODER_PANEL$)?>$MODER_PANEL$<?endif?> 
 </div>
 </div>
 
 </div>
 </div>
</div>